package com.kota.todolist.extension

import android.content.Context
import android.support.v7.widget.TooltipCompat
import android.text.TextUtils
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}
fun Disposable.addTo(disposables: CompositeDisposable) = disposables.add(this)

fun EditText.string() = text.toString()

fun EditText.isEmpty() = TextUtils.isEmpty(text)

fun Context.dpToPx(dp: Float): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics).toInt()
}

fun View.setTooltipCompat(resId: Int) = TooltipCompat.setTooltipText(this, this.context.getString(resId))