package com.kota.todolist.ui.tasklist

import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kota.todolist.R
import com.kota.todolist.data.Task
import com.kota.todolist.extension.hide
import com.kota.todolist.injection.ApplicationContext
import com.kota.todolist.extension.show
import kotlinx.android.synthetic.main.item_task.view.imageViewImportantTaskIndicator
import kotlinx.android.synthetic.main.item_task.view.textViewTodoDescription
import kotlinx.android.synthetic.main.item_task.view.textViewTodoTitle
import javax.inject.Inject

class TaskAdapter @Inject constructor(@ApplicationContext private val context: Context) : RecyclerView.Adapter<TaskAdapter.VH>() {

    private var onItemClickListListener: (Task) -> Unit = {}

    fun setOnItemClickListener(listener: (Task) -> Unit) {
        onItemClickListListener = listener
    }

    var items: MutableList<Task> = mutableListOf()
        set(value) {
            val oldItems = items
            field = value
            DiffUtil.calculateDiff(TodoDiffUtil(value, oldItems)).dispatchUpdatesTo(this)
        }

    override fun onCreateViewHolder(group: ViewGroup, viewType: Int) = VH(LayoutInflater.from(context).inflate(R.layout.item_task, group, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(viewHolder: VH, position: Int) = viewHolder.bind(items[position])

    inner class VH(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(task: Task) {
            with(task) {
                view.textViewTodoTitle.text = title
                view.textViewTodoDescription.text = description
                if (description.isNotEmpty()) view.textViewTodoDescription.show() else view.textViewTodoDescription.hide()
                if (isImportantTask) view.imageViewImportantTaskIndicator.show() else view.imageViewImportantTaskIndicator.hide()
            }
        }
    }

    inner class TodoDiffUtil(private val newList: MutableList<Task>, private val oldList: MutableList<Task>) : DiffUtil.Callback() {

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldList[oldItemPosition] == newList[newItemPosition]

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldList[oldItemPosition].contentEquals(newList[newItemPosition])
    }
}