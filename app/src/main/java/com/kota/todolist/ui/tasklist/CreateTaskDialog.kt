package com.kota.todolist.ui.tasklist

import android.app.AlertDialog
import android.app.Dialog
import android.support.design.widget.TextInputLayout
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.LayoutInflater
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.kota.todolist.R
import com.kota.todolist.data.Task
import com.kota.todolist.extension.string
import com.kota.todolist.stabViewGroup
import com.kota.todolist.ui.base.BaseDialogFragment
import kotlinx.android.synthetic.main.dialog_create_task.view.checkBoxDialogImportantTask
import kotlinx.android.synthetic.main.dialog_create_task.view.editTextDialogDescription
import kotlinx.android.synthetic.main.dialog_create_task.view.editTextDialogTitle
import kotlinx.android.synthetic.main.dialog_create_task.view.imageViewDialogImportantTaskInfo
import kotlinx.android.synthetic.main.dialog_create_task.view.textInputLayoutTitle
import javax.inject.Inject

class CreateTaskDialog @Inject constructor() : BaseDialogFragment() {

    private lateinit var editTextTitle: EditText
    private lateinit var textInputLayoutTitle: TextInputLayout
    private lateinit var editTextDescription: EditText
    private lateinit var checkBoxImportantTask: CheckBox

    private var task: Task = Task.initEmptyTask()
    private var dialogTitleId = R.string.create_task
    private var onPositiveClickListener: (Task) -> Unit = {}

    fun setOnPositiveClickListener(listener: (Task) -> Unit) {
        onPositiveClickListener = listener
    }

    override fun initDialog(): Dialog {
        val view = LayoutInflater.from(activity).inflate(R.layout.dialog_create_task, stabViewGroup)
        editTextTitle = view.editTextDialogTitle
        textInputLayoutTitle = view.textInputLayoutTitle
        editTextDescription = view.editTextDialogDescription
        checkBoxImportantTask = view.checkBoxDialogImportantTask

        view.imageViewDialogImportantTaskInfo.setOnClickListener {
            Toast.makeText(
                    view.context,
                    R.string.todo_important_task_description,
                    Toast.LENGTH_LONG
            ).show()
        }

        editTextTitle.setText(task.title)
        editTextDescription.setText(task.description)
        checkBoxImportantTask.isChecked = task.isImportantTask

        return AlertDialog.Builder(activity, R.style.DialogTheme)
                .setTitle(dialogTitleId)
                .setView(view)
                .setPositiveButton(R.string.save) { _, _ -> }
                .setNegativeButton(android.R.string.no) { _, _ -> }
                .create()
    }

    override fun onStart() {
        super.onStart()
        with(dialog as AlertDialog) {
            window?.setWindowAnimations(R.style.DialogAnimations)
            setCanceledOnTouchOutside(false)
            val positiveButton = getButton(AlertDialog.BUTTON_POSITIVE)
            positiveButton.setTextColor(ContextCompat.getColor(requireActivity(), R.color.colorPrimary))
            positiveButton.setOnClickListener {
                if (TextUtils.isEmpty(editTextTitle.string())) {
                    textInputLayoutTitle.error = getString(R.string.todo_create_empty_title)
                } else {
                    onPositiveClickListener(
                            task.copy(
                                    title = editTextTitle.string(),
                                    description = editTextDescription.string(),
                                    isImportantTask = checkBoxImportantTask.isChecked
                            )
                    )
                    dialog.dismiss()
                }
            }
        }
    }

    fun showEditModeDialog(supportFragmentManager: FragmentManager?, task: Task) {
        if(isDialogShowing()) return
        this.task = task
        dialogTitleId = R.string.edit_task
        super.show(supportFragmentManager, javaClass.name)
    }

    fun showCreateModeDialog(supportFragmentManager: FragmentManager?) {
        if(isDialogShowing()) return
        this.task = Task.initEmptyTask()
        dialogTitleId = R.string.create_task
        super.show(supportFragmentManager, javaClass.name)
    }
}