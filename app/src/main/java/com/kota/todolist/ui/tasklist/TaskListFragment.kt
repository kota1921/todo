package com.kota.todolist.ui.tasklist

import android.arch.lifecycle.ViewModelProviders
import android.graphics.Canvas
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.kota.todolist.App
import com.kota.todolist.EMPTY_PARENT_GROUP_EXCEPTION
import com.kota.todolist.R
import com.kota.todolist.data.Group
import com.kota.todolist.data.observe
import com.kota.todolist.extension.hide
import com.kota.todolist.extension.setTooltipCompat
import com.kota.todolist.extension.show
import kotlinx.android.synthetic.main.fragment_task_list.constraintLayoutEmptyView
import kotlinx.android.synthetic.main.fragment_task_list.imageViewEmptyCreate
import kotlinx.android.synthetic.main.fragment_task_list.recyclerViewTodoList
import javax.inject.Inject

class TaskListFragment : Fragment() {

    @Inject lateinit var presenter: TaskListPresenter
    @Inject lateinit var taskAdapter: TaskAdapter
    @Inject lateinit var createTaskDialog: CreateTaskDialog
    @Inject lateinit var deleteTaskDialog: DeleteTaskDialog
    @Inject lateinit var swipeHelper: SwipeToDeleteCallback

    private val viewModel by lazy { ViewModelProviders.of(this).get(TaskListViewModel::class.java) }
    private val parentGroup: Group by lazy { arguments?.getParcelable<Group>(GROUP_EXTRA) ?: throw RuntimeException(EMPTY_PARENT_GROUP_EXCEPTION) }

    companion object {
        const val GROUP_EXTRA = "com.kota.todolist.group_extra"

        fun createInstance(group: Group): TaskListFragment {
            val bundle = Bundle()
            val fragment = TaskListFragment()
            bundle.putParcelable(GROUP_EXTRA, group)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        App.appComponent.inject(this)
        presenter.attachViewModel(viewModel)
        presenter.initByParentGroup(parentGroup)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_task_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initViewModelObservers()
    }

    override fun onDestroy() {
        presenter.detachViewModel()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.group_list_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_create_task -> presenter.onAddTaskClick()
            else -> return false
        }
        return true
    }

    private fun initViews() {
        with(taskAdapter) {
            setOnItemClickListener { Toast.makeText(context, R.string.app_name, Toast.LENGTH_SHORT).show() }
        }
        with(recyclerViewTodoList) {
            layoutManager = LinearLayoutManager(context)
            adapter = taskAdapter
            ItemTouchHelper(swipeHelper).attachToRecyclerView(this)
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
                    swipeHelper.onDraw(c)
                }
            })
        }
        swipeHelper.setOnDeleteClickListener { presenter.onDeleteTaskClick(it) }
        swipeHelper.setOnEditClickListener { presenter.onEditTaskClick(it) }
        createTaskDialog.setOnPositiveClickListener { presenter.saveTask(it, parentGroup) }
        deleteTaskDialog.setOnPositiveClickListener { presenter.deleteTask(it) }
        imageViewEmptyCreate.setOnClickListener { presenter.onAddTaskClick() }
        imageViewEmptyCreate.setTooltipCompat(R.string.create_task)
    }

    private fun initViewModelObservers() {
        with(viewModel) {
            tasksData.observe(this@TaskListFragment) { taskAdapter.items = it }
            tasksEmptyData.observe(this@TaskListFragment) { if (it) constraintLayoutEmptyView.show() else constraintLayoutEmptyView.hide() }
            createTaskEvent.observe(this@TaskListFragment) { createTaskDialog.showCreateModeDialog(requireFragmentManager()) }
            editTaskEvent.observe(this@TaskListFragment) { createTaskDialog.showEditModeDialog(requireFragmentManager(), it) }
            deleteTaskEvent.observe(this@TaskListFragment) { deleteTaskDialog.show(childFragmentManager, it) }
        }
    }
}