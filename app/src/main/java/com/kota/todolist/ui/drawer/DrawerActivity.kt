package com.kota.todolist.ui.drawer

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import com.kota.todolist.App
import com.kota.todolist.R
import com.kota.todolist.data.Group
import com.kota.todolist.data.observe
import com.kota.todolist.extension.setTooltipCompat
import com.kota.todolist.ui.tasklist.TaskListFragment
import kotlinx.android.synthetic.main.activity_drawer.drawerLayout
import kotlinx.android.synthetic.main.activity_drawer.toolbar
import kotlinx.android.synthetic.main.navigation_drawer.imageViewCreateGroup
import kotlinx.android.synthetic.main.navigation_drawer.recyclerViewNavigationGroups
import javax.inject.Inject

class DrawerActivity : AppCompatActivity(), OnCloseDrawerListener {

    @Inject lateinit var presenter: DrawerPresenter
    @Inject lateinit var navigationMenuAdapter: NavigationMenuAdapter
    @Inject lateinit var createGroupDialog: CreateGroupDialog
    @Inject lateinit var deleteGroupDialog: DeleteGroupDialog

    private val viewModel by lazy { ViewModelProviders.of(this).get(DrawerViewModel::class.java) }

    //это сделанно потому что при одновременном показе анимаций закрытия drawer и fragmentDialog и все лагает
    private var onDrawerCloseAction: () -> Unit = {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawer)
        App.appComponent.inject(this)
        presenter.attachViewModel(viewModel)
        initToolbar()
        initViewModelObservers()
        initViews()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachViewModel()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> drawerLayout.openDrawer(GravityCompat.START)
            else -> return false
        }
        return true
    }

    override fun onDrawerClosed(view: View) = onDrawerCloseAction().also { onDrawerCloseAction = {} }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setHomeAsUpIndicator(R.drawable.ic_menu)
            it.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun initViewModelObservers() {
        with(viewModel) {
            groupsData.observe(this@DrawerActivity) { navigationMenuAdapter.groups = it }
            selectedGroupData.observe(this@DrawerActivity) { changeFragment(it) }
            createGroupEvent.observe(this@DrawerActivity) { createGroupDialog.showForCreate(supportFragmentManager) }
            editGroupEvent.observe(this@DrawerActivity) { createGroupDialog.showForEdit(supportFragmentManager, it) }
            deleteGroupEvent.observe(this@DrawerActivity) { deleteGroupDialog.show(supportFragmentManager, it) }
        }
    }

    private fun initViews() {
        with(navigationMenuAdapter) {
            setOnItemClickListener {
                drawerLayout.closeDrawer(GravityCompat.START)
                presenter.onGroupClick(it)
            }
            setOnItemEditClickListener {
                drawerLayout.closeDrawer(GravityCompat.START)
                onDrawerCloseAction = { presenter.onGroupEditClick(it) }
            }
            setOnItemDeleteClickListener {
                drawerLayout.closeDrawer(GravityCompat.START)
                onDrawerCloseAction = { presenter.onGroupDeleteClick(it) }
            }
        }
        with(recyclerViewNavigationGroups) {
            layoutManager = LinearLayoutManager(this@DrawerActivity)
            adapter = navigationMenuAdapter
        }
        with(imageViewCreateGroup) {
            setOnClickListener {
                drawerLayout.closeDrawer(GravityCompat.START)
                onDrawerCloseAction = { presenter.onCreateGroupClick() }
            }
            setTooltipCompat(R.string.create_group)
        }

        createGroupDialog.setOnPositiveClickListener { presenter.saveGroup(it) }
        deleteGroupDialog.setOnPositiveClickListener { presenter.deleteGroup(it) }
        drawerLayout.addDrawerListener(this)
    }

    private fun changeFragment(group: Group) {
        title = group.title
        navigationMenuAdapter.setItemSelected(group)
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, TaskListFragment.createInstance(group))
                .commitNow()
    }

    companion object {
        fun createStartIntent(context: Context) = Intent(context, DrawerActivity::class.java)
    }
}