package com.kota.todolist.ui.splash

import com.kota.todolist.data.Group
import com.kota.todolist.data.room.DataRepository
import com.kota.todolist.data.room.ResourceRepository
import com.kota.todolist.extension.addTo
import com.kota.todolist.ui.base.BasePresenter
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class SplashPresenter @Inject constructor(
        private val dataRepository: DataRepository,
        private val resourceRepository: ResourceRepository
) : BasePresenter<SplashViewModel>() {

    override fun attachViewModel(viewModel: SplashViewModel) {
        super.attachViewModel(viewModel)
        dataRepository.getAllGroup()
                .subscribeOn(Schedulers.io())
                .subscribe({ if (it.isEmpty()) initDefaultGroup() else viewModel.showGroupActivity() }, Timber::e)
                .addTo(disposables)
    }

    private fun initDefaultGroup() {
        dataRepository.upsertGroup(Group(resourceRepository.defaultGroupName))
                .subscribeOn(Schedulers.io())
                .subscribe({ viewModel?.showGroupActivity }, Timber::e)
                .addTo(disposables)
    }
}