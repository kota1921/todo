package com.kota.todolist.ui.tasklist

import com.kota.todolist.data.Group
import com.kota.todolist.data.Task
import com.kota.todolist.data.room.DataRepository
import com.kota.todolist.extension.addTo
import com.kota.todolist.ui.base.BasePresenter
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class TaskListPresenter @Inject constructor(private val dataRepository: DataRepository) : BasePresenter<TaskListViewModel>() {

    fun initByParentGroup(parentGroup: Group) {
        Flowable.just(true)
                .flatMap { dataRepository.getTasksInGroup(parentGroup) }
                .subscribeOn(Schedulers.io())
                .subscribe({ viewModel?.updateTasks(it) }, Timber::e)
                .addTo(disposables)
    }

    fun saveTask(task: Task, parentGroup: Group) {
        if (parentGroup.id == null) return
        dataRepository.upsertTask(task.copy(parentGroupId = parentGroup.id))
                .subscribeOn(Schedulers.computation())
                .subscribe({}, Timber::e)
                .addTo(disposables)
    }

    fun deleteTask(task: Task) {
        dataRepository.deleteTask(task)
                .subscribeOn(Schedulers.io())
                .subscribe({}, Timber::e)
                .addTo(disposables)
    }

    fun onAddTaskClick() = viewModel?.showCreateDialog()

    fun onDeleteTaskClick(position: Int) = tryGetTask(position)?.let { viewModel?.showDeleteTaskDialog(it) }

    fun onEditTaskClick(position: Int) = tryGetTask(position)?.let { viewModel?.showEditTaskDialog(it) }

    private fun tryGetTask(position: Int): Task? {
        val todoList = viewModel?.tasksData?.value ?: return null
        if (todoList.size <= position) return null
        return todoList[position]
    }
}