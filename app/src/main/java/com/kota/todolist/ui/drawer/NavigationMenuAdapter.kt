package com.kota.todolist.ui.drawer

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kota.todolist.R
import com.kota.todolist.data.Group
import com.kota.todolist.extension.hide
import com.kota.todolist.extension.setTooltipCompat
import com.kota.todolist.extension.show
import kotlinx.android.synthetic.main.item_navigation_menu.view.imageViewDeleteGroup
import kotlinx.android.synthetic.main.item_navigation_menu.view.imageViewEditGroup
import kotlinx.android.synthetic.main.item_navigation_menu.view.textViewGroupName
import javax.inject.Inject

class NavigationMenuAdapter @Inject constructor() : RecyclerView.Adapter<NavigationMenuAdapter.VH>() {

    private var onItemClickListListener: (Group) -> Unit = {}
    private var onItemDeleteClickListener: (Group) -> Unit = {}
    private var onItemEditClickListener: (Group) -> Unit = {}

    private var isRemoveIconVisible: Boolean = true

    private var lastSelectedItemId: Long? = null

    var groups: MutableList<Group> = mutableListOf()
        set(value) {
            field = value
            lastSelectedItemId = field.firstOrNull()?.id
            isRemoveIconVisible = field.size != 1
            notifyDataSetChanged()
        }

    fun setItemSelected(group: Group){
        lastSelectedItemId = group.id
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(listener: (Group) -> Unit) {
        onItemClickListListener = listener
    }

    fun setOnItemDeleteClickListener(listener: (Group) -> Unit) {
        onItemDeleteClickListener = listener
    }

    fun setOnItemEditClickListener(listener: (Group) -> Unit) {
        onItemEditClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): VH =
            VH(LayoutInflater.from(parent.context).inflate(R.layout.item_navigation_menu, parent, false))

    override fun getItemCount(): Int = groups.size

    override fun onBindViewHolder(viewHolder: VH, pos: Int) = viewHolder.bind(groups[pos])

    inner class VH(view: View) : RecyclerView.ViewHolder(view) {

        private val selectedGroupColor = ContextCompat.getColor(view.context, R.color.white)
        private val unselectedGroupColor = ContextCompat.getColor(view.context, R.color.colorPrimaryText)

        fun bind(group: Group) {
            with(itemView) {
                textViewGroupName.text = group.title
                itemView.setOnClickListener {
                    lastSelectedItemId = group.id
                    onItemClickListListener(group)
                    notifyDataSetChanged()
                }
                imageViewEditGroup.setOnClickListener { onItemEditClickListener(group) }
                imageViewEditGroup.setTooltipCompat(R.string.edit_group)
                if (isRemoveIconVisible) imageViewDeleteGroup.show() else imageViewDeleteGroup.hide()
                imageViewDeleteGroup.setOnClickListener { onItemDeleteClickListener(group) }
                imageViewDeleteGroup.setTooltipCompat(R.string.delete_group)
                lastSelectedItemId?.let { textViewGroupName.setTextColor(if (it == group.id) selectedGroupColor else unselectedGroupColor) }
            }
        }
    }
}