package com.kota.todolist.ui.drawer

import android.app.AlertDialog
import android.app.Dialog
import android.support.design.widget.TextInputLayout
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.widget.EditText
import com.kota.todolist.R
import com.kota.todolist.data.Group
import com.kota.todolist.extension.isEmpty
import com.kota.todolist.extension.string
import com.kota.todolist.stabViewGroup
import com.kota.todolist.ui.base.BaseDialogFragment
import kotlinx.android.synthetic.main.dialog_create_group.view.editTextDialogGroupName
import kotlinx.android.synthetic.main.dialog_create_group.view.textInputLayoutGroupName
import javax.inject.Inject

class CreateGroupDialog @Inject constructor() : BaseDialogFragment() {

    private lateinit var textInputLayoutGroupName: TextInputLayout
    private lateinit var editTextDialogGroupName: EditText

    private var group: Group = Group.initEmptyGroup()
    private var dialogTitleId = R.string.create_group
        private var onPositiveClickListener: (Group) -> Unit = {}

    fun setOnPositiveClickListener(listener: (Group) -> Unit) {
        onPositiveClickListener = listener
    }

    override fun initDialog(): Dialog {
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_create_group, stabViewGroup)
        textInputLayoutGroupName = view.textInputLayoutGroupName
        editTextDialogGroupName = view.editTextDialogGroupName

        editTextDialogGroupName.append(group.title)

        return AlertDialog.Builder(activity, R.style.DialogTheme)
                .setView(view)
                .setTitle(dialogTitleId)
                .setPositiveButton(R.string.save, null)
                .setNegativeButton(android.R.string.no) { _, _ -> }
                .create()
    }

    override fun onStart() {
        super.onStart()
        with(dialog as AlertDialog) {
            window?.setWindowAnimations(R.style.DialogAnimations)
            setCanceledOnTouchOutside(false)
            val positiveButton = getButton(AlertDialog.BUTTON_POSITIVE)
            positiveButton.setTextColor(ContextCompat.getColor(requireActivity(), R.color.colorPrimary))
            positiveButton.setOnClickListener {
                if (editTextDialogGroupName.isEmpty()) {
                    textInputLayoutGroupName.error = getString(R.string.todo_create_empty_title)
                } else {
                    onPositiveClickListener(group.copy(title = editTextDialogGroupName.string()))
                    dialog.dismiss()
                }
            }
        }
    }

    fun showForEdit(supportFragmentManager: FragmentManager, groupForEdit: Group) {
        if(isDialogShowing()) return
        group = groupForEdit
        dialogTitleId = R.string.edit_group
        super.show(supportFragmentManager, javaClass.name)
    }

    fun showForCreate(supportFragmentManager: FragmentManager) {
        if(isDialogShowing()) return
        group = Group.initEmptyGroup()
        dialogTitleId = R.string.create_group
        super.show(supportFragmentManager, javaClass.name)
    }
}