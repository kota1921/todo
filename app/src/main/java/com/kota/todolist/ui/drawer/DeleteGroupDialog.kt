package com.kota.todolist.ui.drawer

import android.app.AlertDialog
import android.app.Dialog
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import com.kota.todolist.R
import com.kota.todolist.data.Group
import com.kota.todolist.ui.base.BaseDialogFragment
import javax.inject.Inject

class DeleteGroupDialog @Inject constructor() : BaseDialogFragment() {

    private var onPositiveClickListener: (Group) -> Unit = {}
    private lateinit var objectToDelete: Group

    fun setOnPositiveClickListener(listener: (Group) -> Unit) {
        onPositiveClickListener = listener
    }

    override fun initDialog(): Dialog {
        return AlertDialog.Builder(activity, R.style.DialogTheme)
                .setTitle(R.string.delete_group)
                .setMessage(R.string.delete_group_message)
                .setPositiveButton(R.string.todo_remove_confirm_dialog_ok) { _, _ -> if (this::objectToDelete.isInitialized) onPositiveClickListener(objectToDelete) }
                .setNegativeButton(android.R.string.no) { _, _ -> }
                .create()
    }

    override fun onStart() {
        super.onStart()
        with(dialog as AlertDialog) {
            setCanceledOnTouchOutside(false)
            getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(requireActivity(), R.color.red))
        }
    }

    fun show(supportFragmentManager: FragmentManager, group: Group) {
        super.show(supportFragmentManager, javaClass.name)
        this.objectToDelete = group
    }
}