package com.kota.todolist.ui.splash

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.kota.todolist.App
import com.kota.todolist.ui.drawer.DrawerActivity
import javax.inject.Inject

class SplashActivity : AppCompatActivity() {

    @Inject lateinit var presenter: SplashPresenter

    private val viewModel by lazy { ViewModelProviders.of(this).get(SplashViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
        presenter.attachViewModel(viewModel)
        initViewModelObservers()
    }

    override fun onDestroy() {
        presenter.detachViewModel()
        super.onDestroy()
    }

    private fun initViewModelObservers() {
        viewModel.showGroupActivity.observe(this) { startActivity(DrawerActivity.createStartIntent(this)) }
    }
}