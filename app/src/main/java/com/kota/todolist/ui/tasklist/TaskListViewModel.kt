package com.kota.todolist.ui.tasklist

import android.arch.lifecycle.ViewModel
import com.kota.todolist.data.NonNullMediatorLiveData
import com.kota.todolist.data.SingleLiveEvent
import com.kota.todolist.data.Task
import timber.log.Timber

class TaskListViewModel : ViewModel() {

    val tasksData: NonNullMediatorLiveData<MutableList<Task>> = NonNullMediatorLiveData()
    val tasksEmptyData: NonNullMediatorLiveData<Boolean> = NonNullMediatorLiveData()
    val createTaskEvent: SingleLiveEvent<Any> = SingleLiveEvent()
    val editTaskEvent: SingleLiveEvent<Task> = SingleLiveEvent()
    val deleteTaskEvent: SingleLiveEvent<Task> = SingleLiveEvent()

    fun updateTasks(taskList: MutableList<Task>) {
        Timber.d("update tasks $taskList")
        tasksData.postValue(taskList)
                .also { tasksEmptyData.postValue(taskList.isEmpty()) }
    }

    fun showCreateDialog() = createTaskEvent.postValue(true)

    fun showEditTaskDialog(task: Task) = editTaskEvent.postValue(task)

    fun showDeleteTaskDialog(task: Task) = deleteTaskEvent.postValue(task)
}