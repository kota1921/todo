package com.kota.todolist.ui.drawer

import android.arch.lifecycle.ViewModel
import com.kota.todolist.data.Group
import com.kota.todolist.data.NonNullMediatorLiveData
import com.kota.todolist.data.SingleLiveEvent

class DrawerViewModel : ViewModel() {

    val groupsData: NonNullMediatorLiveData<MutableList<Group>> = NonNullMediatorLiveData()
    val selectedGroupData: NonNullMediatorLiveData<Group> = NonNullMediatorLiveData()
    val createGroupEvent: SingleLiveEvent<Any> = SingleLiveEvent()
    val editGroupEvent: SingleLiveEvent<Group> = SingleLiveEvent()
    val deleteGroupEvent: SingleLiveEvent<Group> = SingleLiveEvent()

    fun updateGroups(groups: MutableList<Group>) {
        val oldList: List<Group>? = groupsData.value
        groupsData.postValue(groups)
        if (oldList == null) {
            selectedGroupData.postValue(groups.first())
        } else {
            when {
                oldList.size < groups.size -> selectedGroupData.postValue(groups.last())
                oldList.size > groups.size -> selectedGroupData.postValue(groups.first())
            }
        }
    }

    fun updateSelectedGroup(group: Group) {
        selectedGroupData.postValue(group)
    }

    fun showCreateGroupDialog() = createGroupEvent.postValue(Any())

    fun showEditGroupDialog(group: Group) = editGroupEvent.postValue(group)

    fun showDeleteGroupDialog(group: Group) = deleteGroupEvent.postValue(group)
}