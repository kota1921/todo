package com.kota.todolist.ui.tasklist

import android.app.AlertDialog
import android.app.Dialog
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import com.kota.todolist.R
import com.kota.todolist.data.Task
import com.kota.todolist.ui.base.BaseDialogFragment
import javax.inject.Inject

class DeleteTaskDialog @Inject constructor() : BaseDialogFragment() {

    private var onPositiveClickListener: (Task) -> Unit = {}
    private lateinit var task: Task

    fun setOnPositiveClickListener(listener: (Task) -> Unit) {
        onPositiveClickListener = listener
    }

    override fun initDialog(): Dialog {
        return AlertDialog.Builder(activity, R.style.DialogTheme)
                .setTitle(R.string.delete_task)
                .setMessage(R.string.delete_task_message)
                .setPositiveButton(R.string.todo_remove_confirm_dialog_ok) { _, _ -> if (this::task.isInitialized) onPositiveClickListener(task) }
                .setNegativeButton(android.R.string.no) { _, _ -> }
                .create()
    }

    override fun onStart() {
        super.onStart()
        with(dialog as AlertDialog) {
            setCanceledOnTouchOutside(false)
            getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(requireActivity(), R.color.red))
        }
    }

    fun show(supportFragmentManager: FragmentManager, task: Task) {
        super.show(supportFragmentManager, javaClass.name)
        this.task = task
    }
}