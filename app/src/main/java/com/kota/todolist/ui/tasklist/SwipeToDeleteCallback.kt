package com.kota.todolist.ui.tasklist

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import com.kota.todolist.R
import android.graphics.RectF
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.support.v7.widget.helper.ItemTouchHelper.ACTION_STATE_SWIPE
import android.support.v7.widget.helper.ItemTouchHelper.LEFT
import android.support.v7.widget.helper.ItemTouchHelper.RIGHT
import android.view.MotionEvent
import com.kota.todolist.extension.dpToPx
import com.kota.todolist.injection.ApplicationContext
import javax.inject.Inject

class SwipeToDeleteCallback @Inject constructor(@ApplicationContext private val context: Context) :
        ItemTouchHelper.Callback() {

    private enum class ActionButtonVisibleState { GONE, SHOW_EDIT, SHOW_DELETE }

    private val widthToActivateButton = 150f
    private val buttonWithWithoutPadding = widthToActivateButton - 20
    private val actionButtonCornerRadius = 8f
    private val iconHalfSize = context.dpToPx(10f)
    private val deleteIcon = ContextCompat.getDrawable(context, R.drawable.ic_delete_white)!!
    private val deletePaint by lazy {
        val deletePaint = Paint()
        deletePaint.color = ContextCompat.getColor(context, R.color.red)
        return@lazy deletePaint
    }
    private val editIcon = ContextCompat.getDrawable(context, R.drawable.ic_edit_white)!!
    private val editPaint by lazy {
        val editPaint = Paint()
        editPaint.color = ContextCompat.getColor(context, R.color.colorPrimary)
        return@lazy editPaint
    }

    private var swipeBack = false
    private var actionButtonState = ActionButtonVisibleState.GONE
    private var swipedViewHolder: RecyclerView.ViewHolder? = null
    private var lastShowedButtonRect: RectF? = null
    private var onEditClickListener: (Int) -> Unit = {}
    private var onDeleteClickListener: (Int) -> Unit = {}

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) =
            makeMovementFlags(0, LEFT or RIGHT)

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
        if (swipeBack) {
            swipeBack = actionButtonState != ActionButtonVisibleState.GONE
            return 0
        }
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }

    override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder) = false

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, p1: Int) {
    }

    override fun onChildDraw(
            c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
            dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean
    ) {
        if (actionState == ACTION_STATE_SWIPE) {
            if (actionButtonState != ActionButtonVisibleState.GONE) {
                var localDx = 0f
                if (actionButtonState == ActionButtonVisibleState.SHOW_DELETE) localDx = Math.min(dX, -widthToActivateButton)
                if (actionButtonState == ActionButtonVisibleState.SHOW_EDIT) localDx = Math.max(dX, widthToActivateButton)
                super.onChildDraw(c, recyclerView, viewHolder, localDx, dY, actionState, isCurrentlyActive)
            } else {
                setTouchListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
            if (actionButtonState == ActionButtonVisibleState.GONE) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }

        swipedViewHolder = viewHolder
    }

    //suppress it because we don't need know about perform click
    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchListener(
            c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
            dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean
    ) {
        recyclerView.setOnTouchListener { _, motionEvent ->
            swipeBack = motionEvent.action in arrayOf(MotionEvent.ACTION_CANCEL, MotionEvent.ACTION_UP)
            if (swipeBack) {
                if (dX < -widthToActivateButton) actionButtonState = ActionButtonVisibleState.SHOW_DELETE
                else if (dX > widthToActivateButton) actionButtonState = ActionButtonVisibleState.SHOW_EDIT
                if (actionButtonState != ActionButtonVisibleState.GONE) {
                    setTouchDownListener(c, recyclerView, viewHolder, dY, actionState, isCurrentlyActive)
                    setItemsClickable(recyclerView, false)
                }
            }

            return@setOnTouchListener false
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchDownListener(
            c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
            dY: Float, actionState: Int, isCurrentlyActive: Boolean
    ) {
        recyclerView.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN)
                setTouchUpListener(c, recyclerView, viewHolder, dY, actionState, isCurrentlyActive)
            return@setOnTouchListener false
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchUpListener(
            c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
            dY: Float, actionState: Int, isCurrentlyActive: Boolean
    ) {
        recyclerView.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                super.onChildDraw(c, recyclerView, viewHolder, 0F, dY, actionState, isCurrentlyActive)
                recyclerView.setOnTouchListener { _, _ -> false }
                setItemsClickable(recyclerView, true)
                swipeBack = false
                if (lastShowedButtonRect?.contains(motionEvent.x, motionEvent.y) == true) handleOnActionButtonClick(viewHolder.adapterPosition)
                actionButtonState = ActionButtonVisibleState.GONE
                swipedViewHolder = null
            }
            return@setOnTouchListener false
        }
    }

    private fun setItemsClickable(recyclerView: RecyclerView, isClickable: Boolean) {
        (0 until recyclerView.childCount).forEach { i -> recyclerView.getChildAt(i).isClickable = isClickable }
    }

    private fun handleOnActionButtonClick(position: Int) {
        when (actionButtonState) {
            ActionButtonVisibleState.SHOW_DELETE -> onDeleteClickListener(position)
            ActionButtonVisibleState.SHOW_EDIT -> onEditClickListener(position)
            else -> return
        }
    }

    private fun drawActionButtons(canvas: Canvas, viewHolder: RecyclerView.ViewHolder?) {
        if (viewHolder == null) return

        val itemView = viewHolder.itemView

        val left = itemView.left
        val right = itemView.right
        val top = itemView.top
        val bottom = itemView.bottom
        val halfHeight = itemView.height / 2
        val iconTopBound = top + halfHeight - iconHalfSize
        val iconBottomBound = bottom - halfHeight + iconHalfSize

        val editButtonRect = RectF(left.toFloat(), top.toFloat(), left + buttonWithWithoutPadding, bottom.toFloat())
        canvas.drawRoundRect(editButtonRect, actionButtonCornerRadius, actionButtonCornerRadius, editPaint)
        val editIconHalfWidth = ((left + widthToActivateButton) / 2).toInt()
        editIcon.setBounds(editIconHalfWidth - iconHalfSize, iconTopBound, editIconHalfWidth + iconHalfSize, iconBottomBound)
        editIcon.draw(canvas)

        val deleteButtonRect = RectF(right - buttonWithWithoutPadding, top.toFloat(), right.toFloat(), bottom.toFloat())
        canvas.drawRoundRect(deleteButtonRect, actionButtonCornerRadius, actionButtonCornerRadius, deletePaint)
        val deleteIconHalfWidth = right - (buttonWithWithoutPadding / 2).toInt()
        deleteIcon.setBounds(deleteIconHalfWidth - iconHalfSize, iconTopBound, deleteIconHalfWidth + iconHalfSize, iconBottomBound)
        deleteIcon.draw(canvas)

        lastShowedButtonRect = null
        if (actionButtonState == ActionButtonVisibleState.SHOW_DELETE) lastShowedButtonRect = deleteButtonRect
        if (actionButtonState == ActionButtonVisibleState.SHOW_EDIT) lastShowedButtonRect = editButtonRect
    }

    fun setOnEditClickListener(listener: (Int) -> Unit) {
        onEditClickListener = listener
    }

    fun setOnDeleteClickListener(listener: (Int) -> Unit) {
        onDeleteClickListener = listener
    }

    fun onDraw(c: Canvas) {
        drawActionButtons(c, swipedViewHolder)
    }

}