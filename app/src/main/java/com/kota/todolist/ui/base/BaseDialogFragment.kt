package com.kota.todolist.ui.base

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment

abstract class BaseDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        retainInstance = true
        return initDialog()
    }

    abstract fun initDialog(): Dialog

    fun isDialogShowing() = dialog != null && dialog.isShowing

    override fun onDestroyView() {
        val dialog = dialog
        // handles https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && retainInstance) {
            dialog.setDismissMessage(null)
        }
        super.onDestroyView()
    }
}