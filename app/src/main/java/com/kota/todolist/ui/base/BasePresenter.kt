package com.kota.todolist.ui.base

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<VM : ViewModel> {

    protected val disposables = CompositeDisposable()

    protected var viewModel: VM? = null
        private set

    open fun attachViewModel(viewModel: VM) {
        this.viewModel = viewModel
    }

    open fun detachViewModel() {
        disposables.clear()
        this.viewModel = null
    }
}