package com.kota.todolist.ui.drawer

import android.support.v4.widget.DrawerLayout
import android.view.View

interface OnCloseDrawerListener : DrawerLayout.DrawerListener {
    override fun onDrawerStateChanged(p0: Int) {}

    override fun onDrawerSlide(p0: View, p1: Float) {}

    override fun onDrawerOpened(p0: View) {}
}