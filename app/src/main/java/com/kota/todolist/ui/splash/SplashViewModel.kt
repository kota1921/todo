package com.kota.todolist.ui.splash

import android.arch.lifecycle.ViewModel
import com.kota.todolist.data.SingleLiveEvent

class SplashViewModel : ViewModel() {
    val showGroupActivity: SingleLiveEvent<Any> = SingleLiveEvent()

    fun showGroupActivity() = showGroupActivity.postValue(Any())
}