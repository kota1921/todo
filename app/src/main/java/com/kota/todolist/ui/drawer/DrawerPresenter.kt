package com.kota.todolist.ui.drawer

import com.kota.todolist.data.Group
import com.kota.todolist.data.room.DataRepository
import com.kota.todolist.extension.addTo
import com.kota.todolist.ui.base.BasePresenter
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class DrawerPresenter @Inject constructor(private val dataRepository: DataRepository) :
        BasePresenter<DrawerViewModel>() {

    override fun attachViewModel(viewModel: DrawerViewModel) {
        super.attachViewModel(viewModel)
        Flowable.just(true)
                .flatMap { dataRepository.getAllGroup() }
                .subscribeOn(Schedulers.io())
                .subscribe(viewModel::updateGroups, Timber::e)
                .addTo(disposables)
    }

    fun onGroupClick(group: Group) = viewModel?.updateSelectedGroup(group)

    fun onCreateGroupClick() = viewModel?.showCreateGroupDialog()

    fun onGroupEditClick(group: Group) = viewModel?.showEditGroupDialog(group)

    fun onGroupDeleteClick(group: Group) = viewModel?.showDeleteGroupDialog(group)

    fun saveGroup(group: Group) =
            dataRepository.upsertGroup(group)
                    .subscribeOn(Schedulers.io())
                    .subscribe({}, Timber::e)
                    .addTo(disposables)

    fun deleteGroup(group: Group) =
            dataRepository.deleteGroup(group)
                    .subscribeOn(Schedulers.io())
                    .subscribe({}, Timber::e)
                    .addTo(disposables)
}