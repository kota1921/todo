package com.kota.todolist.injection.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.kota.todolist.APP_DATABASE_NAME
import com.kota.todolist.data.preference.SharedPreferencesHelper
import com.kota.todolist.data.preference.SharedPreferencesHelperImpl
import com.kota.todolist.data.room.AppDatabase
import com.kota.todolist.data.room.DataRepository
import com.kota.todolist.data.room.ResourceRepository
import com.kota.todolist.injection.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @ApplicationContext
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideResourceRepository(): ResourceRepository = ResourceRepository(application)

    @Provides
    @Singleton
    fun provideSharedPreferencesHelper(): SharedPreferencesHelper = SharedPreferencesHelperImpl(application)

    private val appDatabase: AppDatabase = Room.databaseBuilder(application, AppDatabase::class.java, APP_DATABASE_NAME).build()

    @Singleton
    @Provides
    fun provideDatabaseRepository() = DataRepository(appDatabase)

}