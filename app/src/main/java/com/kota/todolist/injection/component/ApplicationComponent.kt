package com.kota.todolist.injection.component

import android.content.Context
import com.kota.todolist.data.preference.SharedPreferencesHelper
import com.kota.todolist.data.room.DataRepository
import com.kota.todolist.data.room.ResourceRepository
import com.kota.todolist.injection.ApplicationContext
import com.kota.todolist.injection.module.ApplicationModule
import com.kota.todolist.ui.drawer.DrawerActivity
import com.kota.todolist.ui.splash.SplashActivity
import com.kota.todolist.ui.tasklist.TaskListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class)])
interface ApplicationComponent {

    @ApplicationContext
    fun context(): Context

    fun provideDatabaseRepository(): DataRepository
    fun provideResourceRepository(): ResourceRepository
    fun provideSharedPreferencesHelper(): SharedPreferencesHelper

    fun inject(activity: DrawerActivity)
    fun inject(activity: SplashActivity)

    fun inject(detailFragment: TaskListFragment)
}
