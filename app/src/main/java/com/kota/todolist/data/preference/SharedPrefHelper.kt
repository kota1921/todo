package com.kota.todolist.data.preference

import android.content.Context

class SharedPreferencesHelperImpl(private val context: Context) : SharedPreferencesHelper {

    private val sharedPref by lazy { context.getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE) }

    private val sharedPreferencesName = "com.kota.todolist.data.shared_pref"
    private val isFirstGroupAdded = "com.kota.todolist.data.pref_is_first_group_added"

    override var isFirstTimeAddedNewGroup: Boolean
        get() = sharedPref.getBoolean(isFirstGroupAdded, true)
        set(value) = sharedPref.edit().putBoolean(isFirstGroupAdded, value).apply()
}