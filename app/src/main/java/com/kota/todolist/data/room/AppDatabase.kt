package com.kota.todolist.data.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.kota.todolist.data.Group
import com.kota.todolist.data.Task

@Database(entities = [Task::class, Group::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getGroupDao(): GroupDao
    abstract fun getTaskDao(): TaskDao
}