package com.kota.todolist.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Task(
        @ColumnInfo(name = "title") val title: String,
        @ColumnInfo(name = "description") val description: String,
        @ColumnInfo(name = "parentGroupId") val parentGroupId: Long,
        @PrimaryKey(autoGenerate = true) var id: Long? = null,
        @ColumnInfo(name = "importantTask") val isImportantTask: Boolean = false,
        @ColumnInfo(name = "timestamp") val timestamp: Long = System.currentTimeMillis()
) {

    override fun equals(other: Any?): Boolean = hashCode() == other?.hashCode() || (other is Task && id == other.id)

    fun contentEquals(task: Task): Boolean = title == task.title && description == task.description && isImportantTask == task.isImportantTask

    override fun hashCode(): Int = id?.hashCode() ?: super.hashCode()
    override fun toString(): String {
        return "Task(title='$title', description='$description', id=$id, isImportantTask=$isImportantTask, timestamp=$timestamp)"
    }

    companion object {
        fun initEmptyTask() = Task("", "", 0)
    }
}