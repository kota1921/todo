package com.kota.todolist.data.room

import com.kota.todolist.data.Group
import com.kota.todolist.data.Task
import io.reactivex.Completable
import io.reactivex.Flowable

class DataRepository(private val appDatabase: AppDatabase) {

    fun getAllGroup(): Flowable<MutableList<Group>> = appDatabase.getGroupDao().getAll()

    fun upsertGroup(group: Group): Completable = Completable.create { appDatabase.getGroupDao().upsert(group) }

    //todo add cascade delete for group
    fun deleteGroup(group: Group): Completable = Completable.create { appDatabase.getGroupDao().delete(group) }

    fun getTasksInGroup(group: Group): Flowable<MutableList<Task>> =
            appDatabase.getTaskDao().getAllInGroup(group.id ?: Long.MIN_VALUE)
                    .map { it.asSequence().sortedWith(compareByDescending(Task::isImportantTask)).toMutableList() }

    fun upsertTask(task: Task): Completable = Completable.create { appDatabase.getTaskDao().upsert(task) }

    fun deleteTask(task: Task): Completable = Completable.create { appDatabase.getTaskDao().delete(task) }
}