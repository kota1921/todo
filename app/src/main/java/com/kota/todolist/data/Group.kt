package com.kota.todolist.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Group(
        @ColumnInfo(name = "title") val title: String,
        @PrimaryKey(autoGenerate = true) val id: Long? = null
) : Parcelable {

    companion object {
        fun initEmptyGroup() = Group("")
    }
}