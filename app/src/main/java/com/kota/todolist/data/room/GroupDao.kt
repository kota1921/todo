package com.kota.todolist.data.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.kota.todolist.data.Group
import io.reactivex.Flowable

@Dao
interface GroupDao {

    @Query("SELECT * FROM `group`")
    fun getAll(): Flowable<MutableList<Group>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(group: Group): Long

    @Delete
    fun delete(group: Group)
}