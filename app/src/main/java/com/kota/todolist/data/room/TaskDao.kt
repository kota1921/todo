package com.kota.todolist.data.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.kota.todolist.data.Task
import io.reactivex.Flowable

@Dao
interface TaskDao {

    @Query("SELECT * FROM task WHERE parentGroupId=:parentGroupId")
    fun getAllInGroup(parentGroupId: Long): Flowable<MutableList<Task>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(task: Task): Long

    @Delete
    fun delete(task: Task)
}