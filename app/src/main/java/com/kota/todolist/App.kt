package com.kota.todolist

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.kota.todolist.injection.component.ApplicationComponent
import com.kota.todolist.injection.component.DaggerApplicationComponent
import com.kota.todolist.injection.module.ApplicationModule
import dagger.Module
import io.fabric.sdk.android.Fabric
import timber.log.Timber

@Module
class App : Application() {

    companion object {
        lateinit var appComponent: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
        initLogging()
    }

    private fun initDagger() {
        appComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    private fun initLogging() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Fabric.with(this, Crashlytics())
        }
    }
}